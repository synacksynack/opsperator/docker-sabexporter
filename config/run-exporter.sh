#!/bin/sh

EXPORTER_PORT=${EXPORTER_PORT:-9113}
SABNZBD_BASEURL=${SABNZBD_BASEURL:-http://127.0.0.1:8000}

if test "$DEBUG"; then
    set -x
fi
if test -z "$SABNZBD_APIKEY"; then
    echo CRITICAL: missing API Key
    sleep 60
    exit 1
fi

export EXPORTER_PORT SABNZBD_BASEURL

exec python -u /usr/src/app/sabnzbd_exporter.py
